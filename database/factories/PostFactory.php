<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(rand(5,10));
        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'body' => $this->faker->paragraph(rand(200,500)),
            'Cedula' =>$this->faker->ean8,
            'Salida' => $this->faker->city,
            'destino'=>$this->faker->state,
            'numero_asiento'=>$this->faker->randomDigit(1,10),
            'fecha' => $this->faker->date($format = 'd-m-Y', $max = 'now'),
            'hora' => $this->faker->time($format = 'H:i', $max = 'now'),
            'precio' => $this->faker->randomDigit(1,10),
            'user_id' => $this->faker->randomDigit(1,10),
            'category_id' => $this->faker->randomDigit(1,10),
        ];
    }
}
